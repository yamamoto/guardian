.. _daemon:

The :program:`guardian` Daemon
==============================

The core guardian program is the :program:`guardian` daemon.  It is
the program that loads the system module and executes the state
machine described therein. It has three modes of operation::

  guardian [<options>] <module>
  guardian [<options>] <module> <state> [<request>]
  guardian [<options>] [ -i <module>]


system search path
------------------


daemon EPICS interface
^^^^^^^^^^^^^^^^^^^^^^
The daemon creates an EPICS channel access interface through which the
daemon can be controlled and daemon status 

Usually this mode would only be run through the main site supervision
infrastructure (see guardctrl below), but it can be run from the
command line as well.

REQUESTS
--------


INIT state
----------

`goto` states and redirects
---------------------------



execution
---------

daemon mode
^^^^^^^^^^^

The main state machine execution daemon is launched when guardian is
called with a single system name argument::

  guardian [<options>] <module>

In this mode, guardian loads the system module and immediately starts
execution the state graph starting with the 'INIT' state.

The daemon logs to stdout, and is controlled by the guardian EPICS
interface.

single state or path execution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If guardian is called with an additional single state argument, the
guardian executes the single state code until it completes, at which
point guardian exits::

  guardian [<options>] <module> <state>

If two additional state arguments are provided, guardian calculates
the path between the two states on the state graph. Guardian attempts
to execute the path, and exits when the final, request state completes
(or an error or 'jump' is encountered)::

  guardian [<options>] <module> <state> <request>

The daemon EPICS interface is not initialized in this mode.

interactive shell
^^^^^^^^^^^^^^^^^

If no argument is specified, guardian launches a special interactive
shell::

   guardian [<options>]

If a system module is specified with the “interactive” flag, the full
system module will be loaded, and the interactive shell will include
the full environment of the module::

   guardian [<options>] -i <module>

All functions and state methods defined within the system module will
be available directly from the interactive prompt.

The interactive shell is a customized instantiation of `ipython shell
<http://ipython.org/>`_::

  $ guardian
  --------------------
  aLIGO Guardian Shell
  --------------------
  ezca prefix: L1:

  In [1]:

It's useful for testing commands and doing simple math::

  In [1]: ezca['SUS-MC2_M2_LOCK_L_GAIN'] * 6
  Out[1]: 18

It's also useful for viewing built-in documentation::

  In [2]: help(ezca)
