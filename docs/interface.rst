.. _interface:

:program:`guardian`  EPICS Interface
====================================

When :program:`guardian` is run in :ref:`daemon` mode, it creates an
EPICS control interface that can be used to control the process and
inspect it's status.

All channels created by the interface are given the following channel prefix::

    <IFO>:GRD-<NODE>_

For instance, the ``MODE`` channel for the ``IFO_LOCK`` guardian node
for the ``L1`` detector would be::

    L1:GRD-IFO_LOCK_MODE




daemon control/status channels
------------------------------

``OP`` : Operational mode of node
  * 0: `STOP`
  * 1: `PAUSE`
  * 2: `EXEC`


``MODE`` : Execution mode of node.

  `EXEC` and `MANAGED` correspond to active state graph execution modes.

  * 0: `AUTO`
  * 1: `MANAGED`
  * 2: `MANUAL`

``STATE`` : Current state (string)

``STATE_N`` : Current state index (int)

``STATE_S``: Current state string (string)

:``REQUEST``: Index of current requested state  state index (see below)
:``REQUEST_N``:
:``REQUEST_S``: 

:``TARGET_N``: Index of next state in the calculated path  state index (see below)
:``TARGET_N``: Index of next state in the calculated path  state index (see below)
:``TARGET_N``: Index of next state in the calculated path  state index (see below)

:``LOAD``: Usercode load request

:``LOAD_STATUS``:
  * 0: `DONE`
  * 1: `REQUEST`
  * 2: `SET`
  * 3: `INPROGRESS`
  * 4: `ERROR`

:``LOAD_TIME``:


``STATUS`` : Internal state of the node. Indicates which state method
is currently being executed, or if a state transition is occurring.

  * 0: 'ENTER'
  * 1: 'MAIN'
  * 2: 'RUN'
  * 3: 'DONE'
  * 4: 'JUMP'
  * 5: 'REDIRECT'
  * 6: 'EDGE'
  * 7: 'INIT'

``ERROR`` : Non-zero is some sort of execution error has
occurred. 'Connect' errors mean that there is an EPICS communication
error, and that the node is attempting to reestablish connection.  0:
'False', 1: 'True', 3: 'Connect'

``NOTIFICATION`` : Non-zero is there is a operator notification
present in the USERMSG channel. 0: 'False', 1: 'True'
	
``VERSION`` : Current running version of the Guardian core for this
node.  integer (possibly float in the future)


MEDM interface
--------------

.. _fig_medm_main:
.. figure:: _static/MEDM_MAIN.png
   :width: 600px
   :figwidth: 700px
   :align: center

.. _fig_medm_main_opmode:
.. figure:: _static/MEDM_MAIN_OPMODE.png
   :width: 600px
   :figwidth: 700px
   :align: center

.. _fig_medm_states:
.. figure:: _static/MEDM_STATES.png
   :width: 600px
   :figwidth: 700px
   :align: center

.. _fig_medm_main_exectime:
.. figure:: _static/MEDM_MAIN_EXECTIME.png
   :width: 600px
   :figwidth: 700px
   :align: center

.. _fig_medm_main_archive:
.. figure:: _static/MEDM_MAIN_ARCHIVEID.png
   :width: 600px
   :figwidth: 700px
   :align: center

.. _fig_medm_main_managed:
.. figure:: _static/MEDM_MAIN_MANAGED.png
   :width: 600px
   :figwidth: 700px
   :align: center

.. _fig_medm_compact:
.. figure:: _static/MEDM_COMPACT.png
   :width: 600px
   :figwidth: 700px
   :align: center

.. _fig_medm_main_spm:
.. figure:: _static/MEDM_MAIN_SPM_ENABLED.png
   :width: 600px
   :figwidth: 700px
   :align: center

.. _fig_medm_main_spm_enabled:
.. figure:: _static/MEDM_MAIN_SPM_DIFF.png
   :width: 600px
   :figwidth: 700px
   :align: center

.. _fig_medm_spm:
.. figure:: _static/MEDM_SPM.png
   :width: 600px
   :figwidth: 700px
   :align: center

