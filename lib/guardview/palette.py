ui = [
    ('footer', 'white', 'dark blue'),
    ('footer_error', 'white', 'dark red'),
    ('prompt', 'black', 'light green'),

    ('header', '', '', '', 'g62, bold', '#008'),
]

node = [
    ('node name', 'white, bold', '', '', '', ''),
    #('node name focus', 'white', 'dark gray', '', 'white', 'g19'),
    ('node attr', '', '', '', '', ''),
    #('node attr focus', '', 'dark gray', '', '', 'g19'),

    ('ok', '', '', '', '#0f0, bold', ''),
    ('error', '', '', '', '#f00, bold', ''),
    ('connect', '', '', '', '#f80, bold', ''),

    ('indicator green',  '', '', '', '', '#080'),
    ('indicator orange',  '', '', '', '', '#f80'),
    ('indicator red',  '', '', '', '', '#c00'),

    ('state', '', '', '', '#ff0', ''),
    ('state request', '', '', '', '#ff0', ''),
    ('state ok', '', '', '', '#0f0', ''),
    # ('node name', '', ''),
    # ('node name focus', 'white, bold', ''),
    # ('node attr', '', ''),
    # ('node attr focus', 'white, bold', ''),
]

focus_map = {}
node_focus = []
for a in node:
    b = list(a)
    b[0] += ' focus'
    if b[5] == '':
        b[5] = 'g19'
    focus_map[a[0]] = b[0]
    node_focus.append(tuple(b))

PALETTE = ui + node + node_focus
