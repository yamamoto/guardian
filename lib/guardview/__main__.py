import sys

from . import ui

def main():
    nodes = sys.argv[1:]
    ui.UI(nodes=nodes)

if __name__ == '__main__':
    main()
