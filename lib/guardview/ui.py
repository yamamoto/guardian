import os
import urwid
import fnmatch
import collections

import logging
if os.getenv('GUARDCTRL_LOG_FILE', None):
    logging.basicConfig(filename=os.getenv('GUARDCTRL_LOG_FILE'),
                        level=logging.INFO)

from . import palette
from .nodes import NodeItem, BufferNodes
from .help import BufferHelp

############################################################

class UI():

    keys = collections.OrderedDict([
        ('l', "prompt_limit"),
        ('=', "refresh"),
        ('q', "kill_buffer"),
        ('Q', "quit"),
        ('?', "help"),
        ])

    def __init__(self, nodes):
        #self.nodes = nodes
        #self.devnull = open(os.devnull, 'wb') #open('/dev/null', 'rw')
        #self.log = StringIO.StringIO()

        self.header_string = "guardctrl"
        self.status_string = "ENTER: node control, g: node graph, l: limit, q: kill buffer, Q: quit, ?: help"

        # nodes display objects
        node_width = max([len(node) for node in nodes])
        self.nodes = []
        for node in nodes:
            self.nodes.append(NodeItem(self, node, node_width))
        self.resolved_pvs = []

        # make a list to hold the buffer objects
        self.buffers = []

        self.view = urwid.Frame(urwid.SolidFill())
        self.set_status()

        self.new_buffer(BufferNodes(self))
        #self.new_buffer(BufferHelp(self))

        self.mainloop = urwid.MainLoop(
            self.view,
            palette.PALETTE,
            unhandled_input=self.keypress,
            #handle_mouse=False,
            )
        self.mainloop.screen.set_terminal_properties(colors=88)
        self.mainloop.set_alarm_in(2, self.update_ctrl_vars)
        self.mainloop.run()

    def keypress(self, key):
        if key in self.keys:
            logging.debug("KEY: %s %s" % (self.__class__.__name__, key))
            cmd = "self.%s()" % (self.keys[key])
            eval(cmd)
            return True
        else:
            return key

    ##########

    def set_status(self, text=None):
        if not text:
            text = self.status_string
        self.view.set_footer(urwid.AttrMap(urwid.Text(text), 'footer'))

    def error(self, text):
        self.view.set_footer(urwid.AttrMap(urwid.Text(text), 'footer_error'))

    def new_buffer(self, buffer):
        logging.info("new buffer: %s" % buffer)
        self.buffers.append(buffer)
        self.view.body = urwid.AttrMap(buffer, 'body')
        #UI(self.nodes, cmd=cmd, devs=self.devs)
        self.set_status()

    def kill_buffer(self):
        """kill current buffer"""
        if len(self.buffers) <= 1:
            return
            #self.quit()
        self.buffers.pop()
        self.view.body = urwid.AttrMap(self.buffers[-1], 'body')
        self.set_status()

    def prompt(self, string):
        prompt = PromptEdit(string)
        self.view.set_footer(urwid.AttrMap(prompt, 'prompt'))
        self.view.set_focus('footer')
        return prompt

    ##########

    def update_ctrl_vars(self, *args, **kwargs):
        updates = False
        for node in self.nodes:
            for pv in node.dev._pvs.values():
                pvname = pv.pvname
                if pvname in self.resolved_pvs:
                    continue
                if not pv.connected:
                    continue
                logging.info("CTRL UPDATE: %s" % (pvname))
                #pv.get_ctrlvars()
                v = pv.get(as_string=True)
                logging.info("CTRL UPDATE: %s = %s" % (pvname, v))
                pv.run_callbacks()
                self.resolved_pvs.append(pvname)
                updates = True
        if updates:
            self.mainloop.draw_screen()
            self.mainloop.set_alarm_in(2, self.update_ctrl_vars)
            
    def nodes_from_glob(self, glob):
        if not glob:
            return self.nodes
        if type(glob) is str:
            glob = glob.strip().split()
        nodes = []
        for node in self.nodes:
            for s in glob:
                if fnmatch.fnmatch(node.name, s):
                    nodes.append(node)
        return nodes

    def prompt_limit(self):
        """limit node list (with globbing)"""
        prompt = 'limit node list (globbing ok): '
        urwid.connect_signal(self.prompt(prompt), 'done', self._prompt_limit_done)

    def _prompt_limit_done(self, string):
        self.view.set_focus('body')
        urwid.disconnect_signal(self, self.prompt, 'done', self._prompt_limit_done)
        if not string:
            self.set_status()
            return
        buffer = BufferNodes(self, string)
        self.new_buffer(buffer)

    def help(self):
        """help"""
        buffer = BufferHelp(self, self.buffers[-1])
        self.new_buffer(buffer)

    def quit(self):
        """quit"""
        for node in self.nodes:
            for pv in node.dev._pvs.values():
                pv.clear_callbacks()
            self.dev = None
        raise urwid.ExitMainLoop()

############################################################

class PromptEdit(urwid.Edit):
    __metaclass__ = urwid.signals.MetaSignals
    signals = ['done']

    def keypress(self, size, key):
        if key == 'enter':
            urwid.emit_signal(self, 'done', self.get_edit_text())
            return
        elif key == 'esc':
            urwid.emit_signal(self, 'done', None)
            return

        urwid.Edit.keypress(self, size, key)
